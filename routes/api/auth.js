const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const auth = require('../../middleware/auth');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator');

const User = require('../../models/User');

// route --> get api/auth
// access --> public
//Authenticate the user
//this is a protected route
router.get('/', auth, async (req, res) => {
  try {
    //get only id and leave the password

    const user = await User.findById(req.user.id).select('-password');
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server');
  }
});

// route --> post api/authts
// access --> public
//Authenticate the user and login
//this is a protected route

router.post(
  '/',
  [
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'password id required').exists(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    //see if the user exits, send an error
    const { email, password } = req.body;
    try {
      let user = await User.findOne({ email });
      if (!user) {
        return res.status(400).json({ errors: [{ msg: 'User not Found' }] });
      }

      //verify password

      const passwordMatch = await bcrypt.compare(password, user.password);

      if (!passwordMatch) {
        return res.status(400).json({ msg: 'Invalid Credentials' });
      }

      //return the JWT
      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        config.get('jwtSecret'),
        { expiresIn: 360000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (error) {
      console.error(error.message);
      return res.status(500).send('server error');
    }
  }
);
module.exports = router;
