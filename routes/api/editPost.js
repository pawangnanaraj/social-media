const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');

const Post = require('../../models/Post');

//route PUT /api/post/edit/:id
//edit a post
//access private

router.put(
  '/:id',
  auth,
  check('text', 'a text is required').notEmpty(),
  async (req, res) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res.status(400).json({ errors: validationErrors.array() });
    }
    try {
      const post = await Post.findById(req.params.id);
      const authorisedUser = await post.user.toString();

      //find it the post exists
      if (!post) {
        res.status(400).json({ msg: 'Post is not available' });
      }

      //check is the user is authorised
      if (authorisedUser !== req.user.id) {
        res.status(404).json({ msg: 'User is not authorzied' });
      }
      //create the updated post
      post.text = req.body.text;

      //save the post
      await post.save();
      return res.json(post);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

//route PUT /api/post/edit/comment/:id/:comment_id
//edit a post
//access private
router.put('/comment/:id/:comment_id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    //get the comment
    const comment = await post.comments.find(
      (comment) => comment.id === req.params.comment_id
    );
    const authorisedUser = await comment.user.toString();

    //find it the comment exists
    if (!comment) {
      res.status(400).json({ msg: 'Post is not available' });
    }
    console.log(comment.text);
    //check is the user is authorised
    if (authorisedUser !== req.user.id) {
      res.status(404).json({ msg: 'User is not authorzied' });
    }

    //create the updated comment
    comment.text = req.body.text;

    //save the comment
    await comment.save();
    return res.json(comment);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ msg: 'Server Error' });
  }
});

module.exports = router;
