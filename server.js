// importing express
const express = require('express');
const connectDB = require('./config/connectMongoDB');

//initialise our app
const app = express();

//connect to Mongo DB
connectDB();

//Initialising the middleware
app.use(express.json({ extended: false }));

app.get('', (req, res) => res.send('API is working'));

//Define Routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/post', require('./routes/api/post'));
app.use('/api/post/edit', require('./routes/api/editPost'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/authts', require('./routes/api/authTs'));

//It will look for environment variable(env) while deployment
// if there is no env, the port will the local post 5000
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`server has started on port ${PORT}`));
