"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var express = require('express');
var router = express.Router();
var auth = require('../../middleware/auth');
var User = require('../../models/User');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var config = require('config');
var _a = require('express-validator'), check = _a.check, validationResult = _a.validationResult;
// route --> get api/auth
// access --> public
//Authenticate the user and return the id
//this is a protected route
router.get('/', auth, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var userId, err_1, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                //get only id and leave the password
                //const user = req.body;
                console.log(req.user.id);
                return [4 /*yield*/, User.findById(req.user.id).select('-password')];
            case 1:
                userId = _a.sent();
                res.json(userId);
                return [3 /*break*/, 3];
            case 2:
                err_1 = _a.sent();
                result = err_1.Message;
                console.error(result);
                res.status(500).send('Server Error');
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// route --> post api/authts
// access --> public
//Authenticate the user and login
//this is a protected route
router.post('/', [
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'password id required').exists(),
], function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, _a, email, password, user, passwordMatch, payload, err_2, result;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                errors = validationResult(req);
                if (!errors.isEmpty()) {
                    return [2 /*return*/, res.status(400).json({ errors: errors.array() })];
                }
                _a = req.body, email = _a.email, password = _a.password;
                _b.label = 1;
            case 1:
                _b.trys.push([1, 4, , 5]);
                return [4 /*yield*/, User.findOne({ email: email })];
            case 2:
                user = _b.sent();
                if (!user) {
                    return [2 /*return*/, res.status(400).json({ errors: [{ msg: 'User not Found' }] })];
                }
                return [4 /*yield*/, bcrypt.compare(password, user.password)];
            case 3:
                passwordMatch = _b.sent();
                if (!passwordMatch) {
                    return [2 /*return*/, res.status(400).json({ msg: 'Invalid Credentials' })];
                }
                payload = {
                    user: {
                        id: user.id
                    }
                };
                jwt.sign(payload, config.get('jwtSecret'), { expiresIn: 360000 }, function (err, token) {
                    if (err)
                        throw err;
                    res.json({ token: token });
                });
                return [3 /*break*/, 5];
            case 4:
                err_2 = _b.sent();
                result = err_2.Message;
                console.error(result);
                return [2 /*return*/, res.status(500).send('server error')];
            case 5: return [2 /*return*/];
        }
    });
}); });
module.exports = router;
